//
//  ViewController.swift
//  task_countdown
//
//  Created by Andrzej Lapinski on 09.06.2018.
//  Copyright © 2018 Andrzej Lapinski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var percentageLbl: UILabel!
    
    var isTimerOn = false
    var isAnimationStarted = false
    var isAnimationPaused = false
    var timer = Timer()
    var timeLeft: TimeInterval?
    var endTime: Date?
    let shapeLayer = CAShapeLayer()
    var zeroInterval: TimeInterval = 0
    
    private func returnDay() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        let result = formatter.string(from: date)
        print(result)
        return result
    }
    
    private func returnTask() -> String {
        let taskArray = ["Swift", "Go", "JavaScript"]
//        let result = returnDay()
//        if result == "Monday" || result == "Wednesday" || result == "poniedzialek" || result == "poniedziałek" || result == "sroda" || result == "środa" {
//            return "PluralSight"
//        }
        let randomIndex = Int(arc4random_uniform(UInt32(taskArray.count)))
        return taskArray[randomIndex]
    }
    
    
    private func returnInterval() -> Double {
        let result = returnDay()
        if result == "Saturday" || result == "Sunday" || result == "sobota" || result == "niedziela" {
            return 3 * 3600
        }
        else {
            return 2 * 3600
        }
    }
    
    private func returnColor() -> CGColor{
        let colors = [UIColor.red, UIColor.yellow, UIColor.green, UIColor.cyan, UIColor.magenta, UIColor.orange, UIColor.purple]
        let randomIndex = Int(arc4random_uniform(UInt32(colors.count)))
        let color = colors[randomIndex]
        return color.cgColor
    }
    
    @IBOutlet weak var taskNameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        UIScreen.main.brightness = CGFloat(0.1)
        timeLeft = returnInterval()
        percentageLbl.center = self.view.center
        percentageLbl.text = "00:00:00"
        taskNameLbl.text = returnTask()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        let center = view.center
        
        let trackLayer = CAShapeLayer()
        
        let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle: -90.degreesToRadians, endAngle: 270.degreesToRadians, clockwise: true)
        trackLayer.path = circularPath.cgPath
        
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 10
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = kCALineCapRound
        view.layer.addSublayer(trackLayer)
        

        shapeLayer.path = circularPath.cgPath
        
//        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.strokeColor = returnColor()
        shapeLayer.lineWidth = 10
        shapeLayer.strokeEnd = 0
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = kCALineCapRound
        
        view.layer.addSublayer(shapeLayer)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    @objc func updateTime() {
        if timeLeft! > zeroInterval {
            timeLeft = endTime?.timeIntervalSinceNow ?? 0
            percentageLbl.text = timeLeft?.time
        } else {
            percentageLbl.text = "00:00:00"
            timer.invalidate()
        }
    }
    
    @objc private func handleTap() {
        if !isAnimationStarted {
            let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
            basicAnimation.toValue = 1
        
            basicAnimation.duration = returnInterval()
        
            basicAnimation.fillMode = kCAFillModeForwards
            basicAnimation.isRemovedOnCompletion = false
        
            shapeLayer.add(basicAnimation, forKey: "basicAnim")
            
            isAnimationStarted = true
        }
        
        else {
            if !isAnimationPaused {
                let pausedTime = shapeLayer.convertTime(CACurrentMediaTime(), from: nil)
                shapeLayer.speed = 0.0
                shapeLayer.timeOffset = pausedTime
                isAnimationPaused = true
            }
            else {
                    let pausedTime = shapeLayer.timeOffset
                    shapeLayer.speed = 1.0
                    shapeLayer.timeOffset = 0.0
                    shapeLayer.beginTime = 0.0
                    let timeSincePause = shapeLayer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
                    shapeLayer.beginTime = timeSincePause
                isAnimationPaused = false
            }
        }
        
        if !isTimerOn {
            endTime = Date().addingTimeInterval(timeLeft!)
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            isTimerOn = true
        }
        else {
            timer.invalidate()
            isTimerOn = false
        }
        
        
    }
    
    
}

extension TimeInterval {
    var time: String {
        let hours = Int(self/3600)
        var minutes: Int?
        var seconds = Int(ceil(truncatingRemainder(dividingBy: 60)))
        switch hours {
        case 3:
            minutes = Int(self/60) - 180
            break
        case 2:
            minutes = Int(self/60) - 120
        case 1:
            minutes = Int(self/60) - 60
        default:
            minutes = Int(self/60)
            break
        }
        if seconds == 60 {
            seconds = 0
        }
        return String(format:"%02d:%02d:%02d", hours, minutes!,  seconds )
    }
}
extension Int {
    var degreesToRadians : CGFloat {
        return CGFloat(self) * .pi / 180
    }
}

